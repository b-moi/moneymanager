import { Component, Input } from "@angular/core";
import { RateTable } from "src/app/models/rate.model";

@Component({
    selector: 'app-rates-table',
    templateUrl: './rates-table.component.html',
    styleUrls:['./rates-table.component.scss']
})

export class RatesTableComponent{
   
    @Input() tableItems: RateTable;

    constructor(){}

}