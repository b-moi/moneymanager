import { Component } from "@angular/core";
import { AuthService } from "src/app/service/ayth.service";
import { Router } from "@angular/router";

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})

export class HeaderComponent{

    constructor(private authService: AuthService, private router: Router){}

    public get isLogin(): boolean {
        return this.authService.isLogin;
    }

    public login(): void{
        if (this.isLogin){
            this.authService.logaut();
        }else{
            this.router.navigate(['auth']);
        }
    }

    public get userName(): string {
        return this.authService.userName;
    }

}