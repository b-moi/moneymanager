import { NgModule } from "@angular/core";
import { StatisticPageComponent } from "./statistic.component";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

@NgModule({
    declarations:[
        StatisticPageComponent
    ],
    imports:[
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: StatisticPageComponent
            }
        ])
    ]

})

export class StatisticPageModule{}