import { NgModule } from "@angular/core";
import { RatePageComponent } from "./rate.component";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { RateItemComponent } from "./rate-item/rate-item.component";
import { FormsModule } from "@angular/forms";
import { RatesTableComponent } from "../../components/rates-table/rates-table.component";
import { MonthPipe } from "../../pipes/month.pipes";
import { DateTimePipe } from "../../pipes/date.pipes";

@NgModule({
    declarations:[
        RatePageComponent,
        RateItemComponent,
        RatesTableComponent,
        MonthPipe,
        DateTimePipe
    ],
    imports:[
        CommonModule,
        FormsModule,
        RouterModule.forChild([
            {
                path: '',
                component: RatePageComponent
            }
        ])
    ]

})

export class RatePageModule{}