import { Component, OnInit } from "@angular/core";
import { RateItem, RateTable } from "src/app/models/rate.model";
import * as moment from 'moment';

@Component({
    selector: 'app-rate-page',
    templateUrl: './rate.component.html',
    styleUrls: ['./rate.component.scss']
})

export class RatePageComponent implements OnInit{

    private rateNames: string[] = ['Электроэнергия', 'Газ', 'Комунальные платежи', 'Телефон', 'МТС', 'ТВ, Интернет','Приватизация']

    
    public rateItems: RateItem[]=[];
    public rates: RateTable;

    
    constructor(){}

    ngOnInit(){
        const unixDate: number = moment().unix();
        this.rates = {
            date: unixDate,
            items: []
        };
        this.rateNames.forEach(item=>{
            this.rateItems.push({
                date: unixDate,
                name: item,
                price:0
            });
        });
    }

    public onPayConfirm(item: RateItem): void{
        item.date = moment().unix();
        this.rates.items.push(item);
        const index = this.rateItems.indexOf(item);
        this.rateItems.splice(index, 1);
    }

    public get isHaveRates(): boolean{
        return this.rates.items.length>0;
    }

    
}