import { Component, Input, ElementRef, ViewChild, Output, EventEmitter } from "@angular/core";
import { RateItem } from "../../../models/rate.model";

@Component({
    selector: 'app-rate-item',
    templateUrl: './rate-item.component.html',
    styleUrls:['./rate-item.component.scss']
})

export class RateItemComponent{
    
    @Input() item: RateItem;
    @ViewChild('rateRadio') radio: ElementRef;

    @Output() payConfirm = new EventEmitter<RateItem>();

    constructor(){}

    public onPriceSave(): void{
        if(+this.item.price!=0){
            const radio: HTMLInputElement = this.radio.nativeElement;  
            radio.checked = false;
            this.payConfirm.next(this.item);
        }
        
    }



}
